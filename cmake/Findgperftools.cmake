# - Locate gperftools
# Defines:
#
#  GPERFTOOLS_FOUND
#  GPERFTOOLS_INCLUDE_DIR
#  GPERFTOOLS_INCLUDE_DIRS (not cached)
#  GPERFTOOLS_tcmalloc_LIBRARY
#  GPERFTOOLS_profiler_LIBRARY
#  GPERFTOOLS_LIBRARIES (not cached)
#  GPERFTOOLS_LIBRARY_DIRS (not cached)
#  PPROF_EXECUTABLE

find_path(GPERFTOOLS_INCLUDE_DIR NAMES gperftools/tcmalloc.h)
foreach(component tcmalloc profiler)
  find_library(GPERFTOOLS_${component}_LIBRARY NAMES ${component})
  mark_as_advanced(GPERFTOOLS_${component}_LIBRARY)
endforeach()

find_program(PPROF_EXECUTABLE NAMES pprof
             HINTS ${GPERFTOOLS_INCLUDE_DIR}/../bin)

set(GPERFTOOLS_INCLUDE_DIRS ${GPERFTOOLS_INCLUDE_DIR})
set(GPERFTOOLS_LIBRARIES ${GPERFTOOLS_tcmalloc_LIBRARY} ${GPERFTOOLS_profiler_LIBRARY})

# handle the QUIETLY and REQUIRED arguments and set GPERFTOOLS_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(gperftools DEFAULT_MSG GPERFTOOLS_INCLUDE_DIR GPERFTOOLS_LIBRARIES)

mark_as_advanced(GPERFTOOLS_FOUND GPERFTOOLS_INCLUDE_DIR PPROF_EXECUTABLE)

if(GPERFTOOLS_tcmalloc_LIBRARY)
  get_filename_component(GPERFTOOLS_LIBRARY_DIRS ${GPERFTOOLS_tcmalloc_LIBRARY} PATH)
elseif(GPERFTOOLS_profiler_LIBRARY)
  get_filename_component(GPERFTOOLS_LIBRARY_DIRS ${GPERFTOOLS_profiler_LIBRARY} PATH)
endif()

if(PPROF_EXECUTABLE)
  get_filename_component(GPERFTOOLS_BINARY_PATH ${PPROF_EXECUTABLE} PATH)
endif()

find_package(unwind)
set(GPERFTOOLS_ENVIRONMENT PACKAGE unwind)
