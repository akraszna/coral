#ifndef CORALSOCKETS_GENERICSOCKETEXCEPTION_H
#define CORALSOCKETS_GENERICSOCKETEXCEPTION_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral {

  namespace CoralSockets {

    class GenericSocketException : public CoralServerBaseException
    {

    public:

      /// Constructor
      GenericSocketException( const std::string& message,
                              const std::string& methodName = "" )
        : CoralServerBaseException( message, methodName, "coral::CoralSockets" ) {}

      /// Destructor
      virtual ~GenericSocketException() throw() {}

    };

  }

}
#endif
