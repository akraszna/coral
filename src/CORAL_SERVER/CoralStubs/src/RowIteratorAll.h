#ifndef CORALSTUBS_ROWITERATORALL_H
#define CORALSTUBS_ROWITERATORALL_H 1

#include <memory>

// Include files
#include "CoralBase/AttributeList.h"
#include "CoralServerBase/CALOpcode.h"
#include "CoralServerBase/IRequestHandler.h"
#include "CoralServerBase/IRowIterator.h"

// Local include files
#include "SegmentReaderIterator.h"

namespace coral
{

  namespace CoralStubs
  {

    /** @class RowIteratorAll
     *
     *  An implementation of the RowIterator
     *
     *  This class transforms from IByteBufferIterator
     *  to a rowIterator. It uses the SegmentReaderIterator
     *  for handling of segments and decoding/unmarshalling
     *
     *  @author Alexander Kalkhof
     *  @date   2009-04-15
     *///

    class RowIteratorAll : public IRowIterator
    {
    public:

      // Constructor acquires ownership of the reply iterator,
      // but it simply references the row buffer (if any is provided)
      RowIteratorAll( IByteBufferIteratorPtr reply,
                      AttributeList* rowBuffer,
                      CALOpcode opcode );

      virtual ~RowIteratorAll();

      bool nextRow() override;

      const AttributeList& currentRow() const override;

    private:

      /// Copy constructor is private (fix Coverity MISSING_COPY)
      RowIteratorAll( const RowIteratorAll& rhs );

      /// Assignment operator is private (fix Coverity MISSING_ASSIGN)
      RowIteratorAll& operator=( const RowIteratorAll& rhs );

    private:

      IByteBufferIteratorPtr m_reply;   // iterator over reposnse buffers
      AttributeList* m_obuffer;         // row buffer, non-owned pointer
      bool m_ibuffer;  // true means m_obuffer has valid row data
      bool m_fbuffer;  // true means m_obuffer structure has to be filled from byte buffer
      std::unique_ptr<SegmentReaderIterator> m_sri;
      std::unique_ptr<AttributeList> m_own_buffer;
      bool m_structure;

    };

  }

}
#endif
