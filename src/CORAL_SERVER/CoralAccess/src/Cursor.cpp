// Include files
#include <iostream>
//#include "CoralServerBase/attributeListToString.h"
#include "CoralServerBase/NotImplemented.h"
#include "CoralServerBase/../src/debug2936.h"

// Local include files
#include "ConnectionProperties.h"
#include "Cursor.h"
#include "SessionProperties.h"
#include "RelationalAccess/SchemaException.h"
#include "logger.h"
#include "monitoring.h"

// Namespace
using namespace coral::CoralAccess;

//-----------------------------------------------------------------------------

Cursor::Cursor( std::shared_ptr<const SessionProperties> sessionProperties,
                const coral::QueryDefinition& queryDef,
                coral::AttributeList* pOutputBuffer,
                unsigned int rowCacheSize,
                unsigned int memoryCacheSize )
  : m_sessionProperties( sessionProperties )
{
  if ( debug2936 ) std::cout << "__Enter Cursor::ctorOB" << std::endl; // debug CORALCOOL-2936
  SCOPED_TIMER( "CoralAccess::Cursor::CursorOB" );
  if ( pOutputBuffer )
    logger << "Create Cursor with non-null output buffer" << endlog;
  else
    logger << "Create Cursor with null output buffer" << endlog;
  std::string enableOpenCursorsEnv = "CORALACCESS_ENABLEOPENCURSORS";
  static int enableOpenCursors = -1;
  if ( enableOpenCursors == -1 ) enableOpenCursors = ( getenv ( enableOpenCursorsEnv.c_str() ) ? 1 : 0 );
  if ( rowCacheSize == 0 && memoryCacheSize == 0 )
  {
    logger << "Cursor with unlimited cache size (in rows and MB): call fetchAllRows" << endlog;
    m_rows = facade().fetchAllRows( m_sessionProperties->sessionID(),
                                    queryDef,
                                    pOutputBuffer );
  }
  else if ( m_sessionProperties->fromProxy() )
  {
    logger << "Cursor with limited cache size (in rows and/or MB), but fromProxy=true: call fetchAllRows" << endlog;
    m_rows = facade().fetchAllRows( m_sessionProperties->sessionID(),
                                    queryDef,
                                    pOutputBuffer );
  }
  else if ( enableOpenCursors == 0 )
  {
    logger << "Cursor with limited cache size (in rows and/or MB) and fromProxy=false, but " << enableOpenCursorsEnv << " is not set: call fetchAllRows" << endlog;
    m_rows = facade().fetchAllRows( m_sessionProperties->sessionID(),
                                    queryDef,
                                    pOutputBuffer );
  }
  else
  {
    logger << "Cursor with limited cache size (in rows and/or MB) and fromProxy=false and " << enableOpenCursorsEnv << " is set: call fetchRows" << endlog;
    m_rows = facade().fetchRows( m_sessionProperties->sessionID(),
                                 queryDef,
                                 pOutputBuffer,
                                 rowCacheSize,
                                 memoryCacheSize );
  }
  if ( debug2936 ) std::cout << "__Exit Cursor::ctorOB" << std::endl; // debug CORALCOOL-2936
}

//-----------------------------------------------------------------------------

Cursor::Cursor( std::shared_ptr<const SessionProperties> sessionProperties,
                const coral::QueryDefinition& queryDef,
                const std::map< std::string, std::string > outputTypes,
                unsigned int rowCacheSize,
                unsigned int memoryCacheSize )
  : m_sessionProperties( sessionProperties )
{
  if ( debug2936 ) std::cout << "__Enter Cursor::ctorOT" << std::endl; // debug CORALCOOL-2936
  SCOPED_TIMER( "CoralAccess::Cursor::CursorOT" );
  logger << "Create Cursor with output types" << endlog;
  std::string enableOpenCursorsEnv = "CORALACCESS_ENABLEOPENCURSORS";
  static int enableOpenCursors = -1;
  if ( enableOpenCursors == -1 ) enableOpenCursors = ( getenv ( enableOpenCursorsEnv.c_str() ) ? 1 : 0 );
  if ( rowCacheSize == 0 && memoryCacheSize == 0 )
  {
    logger << "Cursor with unlimited cache size (in rows and MB): call fetchAllRows" << endlog;
    m_rows = facade().fetchAllRows( m_sessionProperties->sessionID(),
                                    queryDef,
                                    outputTypes );
  }
  else if ( m_sessionProperties->fromProxy() )
  {
    logger << "Cursor with limited cache size (in rows and/or MB), but fromProxy=true: call fetchAllRows" << endlog;
    m_rows = facade().fetchAllRows( m_sessionProperties->sessionID(),
                                    queryDef,
                                    outputTypes );
  }
  else if ( enableOpenCursors == 0 )
  {
    logger << "Cursor with limited cache size (in rows and/or MB) and fromProxy=false, but " << enableOpenCursorsEnv << " is not set: call fetchAllRows" << endlog;
    m_rows = facade().fetchAllRows( m_sessionProperties->sessionID(),
                                    queryDef,
                                    outputTypes );
  }
  else
  {
    logger << "Cursor with limited cache size (in rows and/or MB) and fromProxy=false and " << enableOpenCursorsEnv << " is set: call fetchRows" << endlog;
    m_rows = facade().fetchRows( m_sessionProperties->sessionID(),
                                 queryDef,
                                 outputTypes,
                                 rowCacheSize,
                                 memoryCacheSize );
  }
  if ( debug2936 ) std::cout << "__Exit Cursor::ctorOT" << std::endl; // debug CORALCOOL-2936
}

//-----------------------------------------------------------------------------

Cursor::~Cursor()
{
  logger << "Delete Cursor" << endlog;
  close();
}

//-----------------------------------------------------------------------------

bool Cursor::next()
{
  if ( debug2936 ) std::cout << "__Enter Cursor::next" << std::endl; // debug CORALCOOL-2936

  // Fix bug #75094 aka CORALCOOL-952 (cursor::next should fail if transaction is not active)
  if( !m_sessionProperties->isTransactionActive() )
    throw coral::QueryException( m_sessionProperties->domainServiceName(), "Transaction is not active", "Cursor::Next()" ); // NB This triggers crash CORALCOOL-2982
  
  try
  {
    SCOPED_TIMER( "CoralAccess::Cursor::next" );
    if ( !m_rows.get() )
      throw Exception( "The cursor is already closed",
                       "Cursor::next",
                       "coral::CoralAccess" );
    bool nextRow = m_rows->nextRow();
    if ( debug2936 ) std::cout << "__Exit Cursor::next" << std::endl; // debug CORALCOOL-2936
    return nextRow;
  }
  catch( std::exception& e )
  {
    if ( debug2936 ) std::cout << "__Exception caught in Cursor::next: " << e.what() << std::endl; // debug CORALCOOL-2936
    throw;
  }
}

//-----------------------------------------------------------------------------

const coral::AttributeList& Cursor::currentRow() const
{
  if ( !m_rows.get() )
    throw Exception( "The cursor is already closed",
                     "Cursor::currentRow",
                     "coral::CoralAccess" );
  //logger << "Cursor::currentRow: " << attributeListToString( m_rows->currentRow() ) << endlog;
  return m_rows->currentRow();
}

//-----------------------------------------------------------------------------

void Cursor::close()
{
  m_rows.reset(0);
}

//-----------------------------------------------------------------------------

const coral::ICoralFacade& Cursor::facade() const
{
  return m_sessionProperties->facade();
}

//-----------------------------------------------------------------------------
