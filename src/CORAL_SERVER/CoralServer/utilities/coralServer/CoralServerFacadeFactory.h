#ifndef CORALSERVER_CORALSERVERFACADEFACTORY_H
#define CORALSERVER_CORALSERVERFACADEFACTORY_H 1

// Include files
#include "CoralServer/CoralServerFacade.h"
#include "CoralServer/ICoralFacadeFactory.h"
#include "CoralServer/TokenGenerator.h"
#include <dlfcn.h> // Workaround for CORALCOOL-2778 using cmake

namespace coral
{

  namespace CoralServer
  {

    /** @class CoralServerFacadeFactory
     *
     *  Factory of CoralServerFacade's.
     *
     *  The coralServer's ServerStubFactory uses an CoralServerFacadeFactory.
     *  Each ServerStub instantiates its own CoralServerFacade,
     *  which is deleted when the ServerStub is deleted.
     *
     *  @author Andrea Valassi
     *  @date   2009-03-02
     *///

    class CoralServerFacadeFactory : virtual public ICoralFacadeFactory
    {

    public:

      /// Constructor.
      CoralServerFacadeFactory()
      {
        // Workaround for CORALCOOL-2778 using cmake
        auto handle = dlopen("liblcg_CoralServer.so", RTLD_GLOBAL | RTLD_LAZY);
        // hack to avoid
        // warning: ISO C++ forbids casting between pointer-to-function and pointer-to-object
        union {
          void* src;
          factory_function_type dst;
        } p2p;
        p2p.src = dlsym(handle, "_internal_CoralServerFacade_factory_");
        m_factoryFunction = p2p.dst;
      }

      /// Destructor.
      virtual ~CoralServerFacadeFactory(){}

      /// Return a new dedicated coral facade.
      /// The returned facade is non-const (may need to create mutex locks).
      /// This method is non-const (it may need to perform some bookkeeping).
      ICoralFacade* newCoralFacade() override
      {
        // Workaround for CORALCOOL-2778 using cmake
        return m_factoryFunction(&m_tokenPool);
      }

    private:

      /// Copy constructor is private
      CoralServerFacadeFactory( const CoralServerFacadeFactory& rhs );

      /// Assignment operator is private
      CoralServerFacadeFactory& operator=( const CoralServerFacadeFactory& rhs );

    private:

      /// The Token generator
      TokenGenerator m_tokenPool;

      // Workaround for CORALCOOL-2778 using cmake
      typedef ICoralFacade* (*factory_function_type)(ITokenPool* pool);
      factory_function_type m_factoryFunction = nullptr;

    };

  }

}
#endif // CORALSERVER_ICORALFACADEFACTORY_H
