#ifndef CORALMONITOR_STATSTYPE_CPUSAGE_H
#define CORALMONITOR_STATSTYPE_CPUSAGE_H

#ifdef __clang__
#include <pthread.h>
#endif
#include "CoralMonitor/IStatsType.h"

namespace coral {

  class CPUsageData;

  class StatsTypeCPUsage : public IStatsType {
  public:
    //constructor
    StatsTypeCPUsage();

    virtual ~StatsTypeCPUsage();

    void getFiguresDesc(std::vector<std::string>&) override;

    void getFiguresData(std::vector<double>&) override;

  private:

    pthread_mutex_t* m_mutex;

    //would be better to have a static member
    //cannot include CPUusageData.h in public header
    CPUsageData* m_data;

  };

}

#endif
