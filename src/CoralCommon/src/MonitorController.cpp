#include <iostream>

#include <memory>
#include "CoralCommon/MonitorController.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/IMonitoringService.h"
#include "RelationalAccess/IMonitoringReporter.h"
#include "RelationalAccess/SessionException.h"


coral::CoralCommon::MonitorController::MonitorController( std::shared_ptr<const IDevSessionProperties> properties )
  : m_properties( properties )
  , m_domainServiceName( properties->domainServiceName() )
  , m_connectionString( properties->connectionString() )
{
}


coral::CoralCommon::MonitorController::~MonitorController()
{
}


void
coral::CoralCommon::MonitorController::start( coral::monitor::Level level )
{
  coral::ConnectionService connSvc;
  coral::monitor::IMonitoringService* monitoringService = &(connSvc.configuration().monitoringService());
  IDevSessionProperties* properties = const_cast<IDevSessionProperties*>( m_properties.get() );
  properties->setMonitoringService( monitoringService );
  std::cout << "Start CoralCommon monitoring for " << m_connectionString << " at level " << level << std::endl;
  monitoringService->setLevel( m_connectionString,level );
  monitoringService->enable( m_connectionString );
}


void
coral::CoralCommon::MonitorController::stop()
{
  coral::monitor::IMonitoringService* monitoringService = m_properties->monitoringService();
  if ( monitoringService ) 
  {
    std::cout << "Stop CoralCommon monitoring for " << m_connectionString << std::endl;
    monitoringService->disable( m_connectionString );
  }
  IDevSessionProperties* properties = const_cast<IDevSessionProperties*>( m_properties.get() );
  properties->setMonitoringService( 0 );
}


void
coral::CoralCommon::MonitorController::reportToOutputStream( std::ostream& os ) const
{
  coral::monitor::IMonitoringService* monitoringService = m_properties->monitoringService();
  if ( ! monitoringService ) 
  {
    throw coral::MonitoringServiceNotFoundException( m_domainServiceName,
                                                     "IMonitoring::reportToOutputStream" );
  }
  monitoringService->reporter().reportToOutputStream( m_domainServiceName,os );
}


void
coral::CoralCommon::MonitorController::report() const
{
  coral::monitor::IMonitoringService* monitoringService = m_properties->monitoringService();
  if ( ! monitoringService ) 
  {
    throw coral::MonitoringServiceNotFoundException( m_domainServiceName,
                                                     "IMonitoring::report" );
  }
  monitoringService->reporter().report( m_domainServiceName );
}


bool
coral::CoralCommon::MonitorController::isActive() const
{
  return ( m_properties->monitoringService() != 0 ||
           m_properties->monitoringService()->active( m_connectionString ) );
}
